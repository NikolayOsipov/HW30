from tests.factories import ClientFactory, ParkingFactory
from main.models import Client, Parking


def test_create_client(app, db):
    client = ClientFactory()
    db.session.add(client)
    db.session.commit()
    assert client.id is not None
    assert client.car_number is not None
    assert len(db.session.query(Client).all()) == 2


def test_create_parking(client, db):
    parking = ParkingFactory()
    db.session.add(parking)
    db.session.commit()
    assert parking.id is not None
    assert parking.address is not None
    assert len(db.session.query(Parking).all()) == 2
