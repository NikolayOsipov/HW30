import pytest
from main.app import create_app
from main.models import Client, ClientParking, Parking, db as _db
from datetime import datetime


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite://"
    with _app.app_context():
        _db.create_all()
        client = Client(name="John",
                        surname="Doe",
                        credit_card='1234567812345678',
                        car_number="ABC123")
        parking = Parking(address="123 Street",
                          opened=True,
                          count_places=20,
                          count_available_places=10)
        entry_time = datetime(2023, 3, 9, 12, 0, 0)

        clientparking = ClientParking(id=1,
                                      client_id=1,
                                      parking_id=1,
                                      time_in=entry_time,
                                      time_out=None
                                      )

        _db.session.add(client)
        _db.session.add(parking)
        _db.session.add(clientparking)
        _db.session.commit()

        yield _app
        _db.session.close()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        yield _db
