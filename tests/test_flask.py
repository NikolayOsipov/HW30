import pytest
from main.models import Client, Parking, ClientParking


def test_client(client) -> None:
    resp = client.get("/clients/1")
    assert resp.status_code == 200
    assert resp.json == {"id": 1, "name": "John", "surname": "Doe",
                         "car_number": "ABC123",
                         "credit_card": "1234567812345678"}


def test_create_client(client) -> None:
    client_data = {"name": "John", "surname": "Doe",
                   "car_number": "ABC123", "credit_card": "1234567812345678"}
    resp = client.post("/clients", data=client_data)

    assert resp.status_code == 201


@pytest.mark.parking
def test_entry_and_exit_parking(client, app):
    entry_data = {
        'client_id': 1,
        'parking_id': 1
    }
    entry_response = client.post('/entry_parking', data=entry_data)
    assert entry_response.status_code == 201

    exit_data = {
        'client_id': 1,
        'parking_id': 1
    }
    exit_response = client.delete('/exit_parking', data=exit_data)
    assert exit_response.status_code == 200


def test_create_parking(client, app):
    data = {
        'address': 'New Street',
        'opened': True,
        'count_places': 15,
        'count_available_places': 5
    }
    response = client.post('/parking', data=data)
    assert response.status_code == 201

    # Check if the parking was added to the database
    assert Parking.query.filter_by(address='New Street').count() == 1


def test_entry_parking(client, app):
    entry_data = {
        'client_id': 1,
        'parking_id': 1
    }
    response = client.post('/entry_parking', data=entry_data)
    assert response.status_code == 201

    parking = Parking.query.get(1)
    assert parking.count_available_places == 9
    assert ClientParking.query.filter_by(
        client_id=1,
        parking_id=1
    ).count() == 2


def test_exit_parking(client, app):
    exit_data = {
        'client_id': 1,
        'parking_id': 1
    }
    response = client.delete('/exit_parking', data=exit_data)
    assert response.status_code == 200

    parking = Parking.query.get(1)
    assert parking.count_available_places == 11

    client_parking = ClientParking.query.filter_by(
        client_id=1,
        parking_id=1
    ).first()
    assert client_parking.time_out is not None

    client = Client.query.get(1)
    assert client.credit_card is not None


def test_app_config(app):
    assert not app.config['DEBUG']
    assert app.config['TESTING']
    assert app.config['SQLALCHEMY_DATABASE_URI'] == "sqlite://"


@pytest.mark.parametrize("route", ["/clients/1", "/clients"])
def test_route_status(client, route):
    rv = client.get(route)
    assert rv.status_code == 200
