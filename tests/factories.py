import factory.fuzzy

from main.models import Client, Parking
from factory.fuzzy import FuzzyChoice, FuzzyInteger


class ClientFactory(factory.Factory):
    class Meta:
        model = Client

    name = factory.Faker('first_name')
    surname = factory.Faker('last_name')
    credit_card = factory.LazyAttribute(
        lambda o: None if FuzzyChoice(
            [True, False]).fuzz() else '1234567812345678'
    )
    car_number = factory.Faker('text')


class ParkingFactory(factory.Factory):
    class Meta:
        model = Parking

    address = factory.Faker('street_address')
    opened = FuzzyChoice([True, False])
    count_places = FuzzyInteger(1, 100)
    count_available_places = factory.LazyAttribute(lambda o: o.count_places)
