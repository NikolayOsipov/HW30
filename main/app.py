from datetime import datetime
from typing import List, Optional

from flask import Flask, Response, jsonify, request
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///prod.db"
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    db.init_app(app)

    from .models import Client, ClientParking, Parking

    @app.before_request
    def before_request_func():
        db.create_all()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db.session.remove()

    @app.route("/clients", methods=["POST"])
    def create_client_handler():
        """Создание нового клиента"""
        name = request.form.get("name", type=str)
        car_number = request.form.get("car_number", type=str)
        credit_card = request.form.get("credit_card", type=str)
        surname = request.form.get("surname", type=str)

        new_client = Client(
            name=name, surname=surname, credit_card=credit_card, car_number=car_number
        )

        db.session.add(new_client)
        db.session.commit()

        return "", 201

    @app.route("/clients", methods=["GET"])
    def get_clients_handler() -> tuple[Response, int]:
        """Получение клиента"""
        clients: List[Client] = db.session.query(Client).all()
        uclients_list = [u.to_json() for u in clients]
        return jsonify(uclients_list), 200

    @app.route("/clients/<int:client_id>", methods=["GET"])
    def get_client_handler(client_id: int):
        """Получение клиента по id"""
        client: Optional[Client] = db.session.query(Client).get(client_id)
        if client is not None:
            return jsonify(client.to_json()), 200
        else:
            return "Client not found", 404

    @app.route("/parking", methods=["POST"])
    def create_parking_handler():
        """Создание новой парковки"""
        address = request.form.get("address", type=str)
        opened = request.form.get("opened", type=bool)
        count_places = request.form.get("count_places", type=int)
        count_available_places = request.form.get("count_available_places", type=int)

        new_parking = Parking(
            address=address,
            opened=opened,
            count_places=count_places,
            count_available_places=count_available_places,
        )

        db.session.add(new_parking)
        db.session.commit()
        return "", 201

    @app.route("/entry_parking", methods=["POST"])
    def create_entry_parking_handler() -> tuple[dict[str, str], int] or None:
        """Entry parking"""
        client_id = request.form.get("client_id", type=int)
        parking_id = request.form.get("parking_id", type=int)

        parking: Optional[Parking] = db.session.query(Parking).get(parking_id)

        if parking is None:
            return {"message": "Parking not found"}, 404

        if parking.count_available_places <= 0:
            return {"message": "No available parking places"}, 400

        client: Optional[Client] = db.session.query(Client).get(client_id)

        if client is None:
            return {"message": "Client does not exist"}, 404

        if client.credit_card is None or client.credit_card == "":
            return {"message": "Client has no credit card linked"}, 400

        entry_time = datetime.now()

        new_entry = ClientParking(
            client_id=client_id, parking_id=parking_id, time_in=entry_time
        )
        db.session.add(new_entry)

        parking.count_available_places -= 1

        db.session.commit()
        return {"message": "Entry successful"}, 201

    @app.route("/exit_parking", methods=["DELETE"])
    def create_exit_parking_handler() -> tuple[dict[str, str], int]:
        """Exit parking"""
        client_id: int | None = request.form.get("client_id", type=int)
        parking_id: int | None = request.form.get("parking_id", type=int)

        entry: Optional[ClientParking] = (
            db.session.query(ClientParking)
            .filter_by(client_id=client_id, parking_id=parking_id, time_out=None)
            .first()
        )

        if entry is None:
            return {
                "message": "Client has not entered this parking or has already left"
            }, 404

        client: Optional[Client] = db.session.query(Client).get(client_id)

        if client is None:
            return {"message": "Client not found"}, 404

        if not client.credit_card:
            return {
                "message": "Client does not have a linked credit card."
                " Please add a credit card before exiting the "
                "parking."
            }, 400

        entry_time = entry.time_in
        exit_time = datetime.now()
        time_difference = exit_time - entry_time

        if time_difference.total_seconds() < 0:
            return {
                "message": "The exit time cannot be earlier than the entry time."
            }, 404

        hours_parked = time_difference.total_seconds() / 3600
        total_cost = hours_parked * 50
        entry.time_out = exit_time
        parking: Optional[Parking] = db.session.query(Parking).get(parking_id)
        if parking:
            parking.count_available_places += 1
            db.session.commit()
            return {
                "message": f"Exit successful. {total_cost}"
                f" has been charged from your credit card."
            }, 200
        else:
            return {"message": "Parking not found"}, 404

    return app
