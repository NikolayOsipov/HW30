config = "postgresql+psycopg2://admin:admin@localhost/postgres"


class Config:
    SQLALCHEMY_DATABASE_URI = config
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = "secret_key"
    DEBUG = True
